#pragma once
#include "TextObject.h"
#include <iostream>
#include <string>
class WTExtra : public TextObject
{
	std::string description;
	int costPerDie;

public:
	WTExtra(std::string _name, int _costPerDie = 0, std::string _description = "");
	~WTExtra();

	int getCost() const;
	std::string getDescription()  const;

	void displayCost()  const;
	void displayDescription()  const;
	void displayContents()  const;
	bool operator==(const WTExtra& ex) const;
};

