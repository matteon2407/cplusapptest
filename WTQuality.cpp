#include "WTQuality.h"

WTQuality::extraNode::extraNode(WTExtra _extra, int _extraLevels, std::string _extraNotes) : extra{ _extra }, extraNotes{ _extraNotes }, nextExtra{ 0 }, extraLevels{ _extraLevels }
{
}

WTQuality::extraNode::~extraNode()
{
}

std::string WTQuality::qualityEnumToString(QUALITY_TYPE _type) const
{
	switch (_type)
	{
	case Attacks: return "Attacks";
		break;
	case Defends: return "Defends";
		break;
	case Useful: return "Useful";
		break;
	default: return "Unrecognized Quality Type";
	}
}

std::string WTQuality::capacityEnumToString(CAPACITY_TYPE _type) const
{
	switch (_type)
	{
	case Self: return "Self";
		break;
	case Touch: return "Touch";
		break;
	case Mass: return "Mass";
		break;
	case Range: return "Range";
		break;
	case Speed: return "Speed";
		break;
	default: return "Unrecognized Quality Type";
	}
}

WTQuality::WTQuality(std::string _name, QUALITY_TYPE _type, int _numberOfCapacities, CAPACITY_TYPE* _capacitiesArr, int _extraLevels, std::string _description, int _numberOfExtras, WTExtra* _extraArray) :TextObject{ _name },
qType{ _type }, numberOfCapacities{ _numberOfCapacities }, numberOfExtras{_numberOfExtras}, description{_description}, extraLevels{_extraLevels}
{
	capacitiesArr = new CAPACITY_TYPE[numberOfCapacities]; //Create a new array for capacities
	for (int i = 0; i < numberOfCapacities; i++)
	{
		capacitiesArr[i] = _capacitiesArr[i];
	}

	//EXTRAS
	if (numberOfExtras == 0) 
	{
		extraHead = 0;
	}
	else
	{
		extraHead = new extraNode(*(_extraArray)); //Create the first extra, assign it as head
		extraNode* temp = extraHead;
		for (int i = 0; i < numberOfExtras-1; i++) //create the rest of extras and make a list
		{
			temp->nextExtra=new extraNode(*(_extraArray + i)); //add to the end of the list
			temp = temp->nextExtra;//move to the next
		}
	}
	//COST
	costPerDie = 2; //base
	extraNode* temp = extraHead;
	for (int i = 0; i < numberOfExtras; i++)
	{
		costPerDie+=temp->extra.getCost(); //add the cost
		temp = temp->nextExtra; //move to the next extra
	}
	costPerDie += extraLevels;
	if (costPerDie < 1)costPerDie = 1;
}

WTQuality::~WTQuality()
{
	delete[] capacitiesArr; //cleaning up the capacities
	//cleaning the extras:
	if (extraHead != 0)
	{
		extraNode* temp1 = extraHead;
		extraNode* temp2 = temp1->nextExtra;
		while (temp1 != 0)
		{
			temp2 = temp1->nextExtra; //remember the next node
			temp1->~extraNode(); //deleting the node
			temp1 = temp2; //move to the next
		}
	}
}

int WTQuality::getNumberofCapacities() const
{
	return numberOfCapacities;
}

const CAPACITY_TYPE* WTQuality::getCapacitiesArr() const
{
	return capacitiesArr;
}

void WTQuality::displayName() const
{
	std::cout << getName() << " (" << costPerDie << "/die)\n";
}

void WTQuality::displayQualityType() const
{
	std::cout << "Quality: " << qualityEnumToString(qType);
	if (extraLevels > 0)
	{
		std::cout << "+" << extraLevels;
	}
	std::cout<< "\n";
}

void WTQuality::displayCapacities() const
{
	std::cout << "Capacities: ";
	for (int i = 0; i < numberOfCapacities;i++)
	{
	std::cout << capacityEnumToString(capacitiesArr[i])<< " ";
	}
}

void WTQuality::displayContents() const
{
	displayName();
	displayQualityType();
	displayCapacities();
}
