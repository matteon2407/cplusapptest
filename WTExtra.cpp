#include "WTExtra.h"

WTExtra::WTExtra(std::string _name, int _costPerDie, std::string _description) :TextObject{ _name }, costPerDie{ _costPerDie }, description{ _description }
{
}

WTExtra::~WTExtra()
{
}

int WTExtra::getCost() const
{
	return costPerDie;
}

std::string WTExtra::getDescription() const
{
	return description;
}

void WTExtra::displayCost() const
{
	std::cout <<"Cost: "<< costPerDie<<"\n";
}

void WTExtra::displayDescription() const
{
	std::cout << "Description: "<<description << "\n";
}

void WTExtra::displayContents() const
{
	WTExtra::displayName();
	WTExtra::displayCost();
	WTExtra::displayDescription();
	std::cout << "---" ;
}

bool WTExtra::operator==(const WTExtra& ex) const
{
	if ((this->getName()==ex.getName()) and (this->costPerDie==ex.getCost())) return true; //not taking descriptuion into account, just name and cost
		else return false;
}
