#pragma once
#include "TextObject.h"
#include "WTExtra.h"

enum QUALITY_TYPE {Attacks, Defends, Useful};
enum CAPACITY_TYPE{Self, Touch, Mass, Range, Speed};

class WTQuality : public TextObject
{
	std::string description;
	int costPerDie;
	QUALITY_TYPE qType;
	int extraLevels;
	
	class extraNode //Nodes for a single-linked list of extras. Public since it can be used only within the class
	{
	public:
		const WTExtra extra; //extras are NOT to be modified
		std::string extraNotes;
		int extraLevels; //amount of time an extra was taken with this quality (ex. Area 4)
		extraNode* nextExtra;

		extraNode(WTExtra _extra, int _extraLevels=1, std::string _extraNotes="");
		~extraNode();
	};

	int numberOfExtras;
	extraNode* extraHead;

	int numberOfCapacities; //Capacities are done as an array because there is fewer of them and are less frequently changed
	CAPACITY_TYPE* capacitiesArr;

	std::string qualityEnumToString(QUALITY_TYPE _type) const;
	std::string capacityEnumToString(CAPACITY_TYPE _type) const;

public:
	WTQuality(std::string _name, QUALITY_TYPE _type, int _numberOfCapacities, CAPACITY_TYPE* _capacitiesArr, int _extraLevels=0, std::string _description="", int _numberOfExtras=0, WTExtra* _extraArray=0); //general contructor
	~WTQuality();

	int getNumberofCapacities() const;
	const CAPACITY_TYPE* getCapacitiesArr() const;

	void displayName() const;
	void displayQualityType() const;
	void displayCapacities() const;
	void displayContents() const;
};

