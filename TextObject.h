#pragma once
#include <iostream>
#include <string>
class TextObject
{
	std::string name;
public:
	TextObject(std::string _name);
	virtual ~TextObject();

	std::string getName() const;
	virtual void displayName() const;
	virtual void displayContents() const=0;

};

