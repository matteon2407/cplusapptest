#include "TextObject.h"

TextObject::TextObject(std::string _name):name{_name}
{
}

TextObject::~TextObject()
{
}

std::string TextObject::getName() const
{
	return name;
}

void TextObject::displayName() const
{
	std::cout <<"Name: "<< name<<"\n";
}

void TextObject::displayContents() const
{
	TextObject::displayName();
}
